
const express = require('express')
const cors = require('cors')
const mysql = require('mysql2')
const { exit } = require('process')
const connection = mysql.createConnection({
    
    host:'172.17.0.4',
    user:'root',
    password:'root',
    waitForConnections: true,
    
    database:'student'
})


const app = express()
app.use(cors('*'))
app.use(express.json())
connection.connect()

app.get('/',(req,res)=>{
    connection.query('select * from student',(error,result)=>{
        if(error!=null)
            res.send(error)
        else
            res.send(result)
    })
});

app.post('/',(req,res)=>{
    var query = `insert into student values(default,'${req.body.s_name}','${req.body.password}',${req.body.course},'${req.body.passing_year}','${req.body.prn_no}',${req.body.dob})`
    connection.query(query,(error,result)=>{
        if(error!=null)
        res.send(error)
        else
        res.send(result)
    })
});

app.put('/:no',(req,res)=>{
    const query=`update student set s_name='${req.body.s_name}',password='${req.body.password}',course='${req.body.course}' ,passing_year='${req.body.passing_year}' ,prn_no='${req.body.prn_no}' , dob='${req.body.dob}' where student_id=${req.params.no}`;
    connection.query(query,(error,result)=>{
        if(error!=null)
        res.send(error)
        else
        res.send(result)
    })
});

app.delete('/:no',(req,res)=>{
    const query=`delete from student where student_id=${req.params.no}`
    connection.query(query,(error,result)=>{
        if(error!=null)
        res.send(error)
        else
        res.send(result)
    })
});

//////////////////////////////////////
app.listen(3000,'0.0.0.0',()=>{
    console.log("listening to server on 3000")
})